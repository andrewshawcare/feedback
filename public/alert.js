/* global window */
(function () {
    "use strict";
    window.Alert = {
        success: function (message) {
            window.alert(message);
        }
    };
}());