/* global window, document, Persistence, Alert, populateFieldsViaQueryString */
window.onload = function () {
    "use strict";
    var feedbackFormElement = document.getElementById("feedback");

    feedbackFormElement.addEventListener("submit", function (event) {
        event.preventDefault();

        var senderInputElement = document.getElementById("sender");
        var sender = senderInputElement.value;

        var recipientInputElement = document.getElementById("recipient");
        var recipient = recipientInputElement.value;

        var messageInputElement = document.getElementById("message");
        var message = messageInputElement.value;

        Persistence.push({
            sender: sender,
            recipient: recipient,
            message: message
        });

        Alert.success("Thanks for your feedback!");
    });

    populateFieldsViaQueryString({
        queryString: window.location.search.substr(1),
        whitelist: ["sender", "recipient"]
    });
};