/* global window, Firebase */
(function () {
    "use strict";
    var firebase = new Firebase("https://lab-i.firebaseio.com/feedback");
    window.Persistence = {
        push: function (value) {
            value.date = Firebase.ServerValue.TIMESTAMP;
            value.source = "web";
            firebase.push(value);
        },
        pull: function (parameters) {
            var filter = (typeof parameters.filter === "function") ?
                parameters.filter :
                function () { return false; };
            var callback = (typeof parameters.callback === "function") ?
                parameters.callback :
                function () {};
            firebase.on("child_added", function (child) {
                var value = child.val();
                if (filter(value)) {
                    callback(value);
                }
            });
        }
    };
}());