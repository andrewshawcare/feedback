/* global window, document */
(function () {
    "use strict";
    var isValidQueryStringField = function(queryStringField) {
        return queryStringField.match(/^.*=.*$/);
    };

    var parseQueryStringField = function (queryStringField) {
        var pair = queryStringField.split("=");
        return {
            key: pair[0],
            value: pair[1]
        };
    };

    var isFieldInWhitelist = function (whitelist, field) {
        return whitelist.indexOf(field.key) > -1;
    };

    var populateFieldElement = function (field) {
        document.getElementById(field.key).value = field.value;
    };

    window.populateFieldsViaQueryString = function (parameters) {
        var queryString = (typeof parameters.queryString === "string") ? parameters.queryString : "";
        var whitelist = Array.isArray(parameters.whitelist) ? parameters.whitelist : [];

        queryString
            .split("&")
            .filter(isValidQueryStringField)
            .map(parseQueryStringField)
            .filter(isFieldInWhitelist.bind(this, whitelist))
            .map(populateFieldElement);
    };
}());