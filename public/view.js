/* global window, document, Persistence, moment */
window.onload = function () {
    "use strict";
    var feedbackElement = document.getElementById("feedback");
    var recipient = document.location.search.substr(1).split("=")[1];

    var heading = document.createElement("h1");
    heading.innerText = "Feedback for " + recipient;

    document.body.insertBefore(heading, document.body.firstChild);

    Persistence.pull({
        filter: function (value) {
            return value.recipient === recipient;
        },
        callback: function (feedback) {
            var citationElement = document.createElement("cite");
            var messageElement = document.createElement("blockquote");

            citationElement.innerText = moment(feedback.date).fromNow() + (feedback.sender ? " from " + feedback.sender : "");
            messageElement.innerText = feedback.message;

            messageElement.appendChild(citationElement);
            feedbackElement.insertBefore(messageElement, feedbackElement.firstChild);
        }
    });
};